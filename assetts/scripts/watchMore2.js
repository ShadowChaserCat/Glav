let pageNumber = document.querySelectorAll('[data-page-card-number]');
let cardSection = document.querySelector('.card-section');
let fetchSection = document.querySelector('.card-section-fetch');
fetchSection.style.display = 'none';
let page = 2;
let pageBtnLeft = document.querySelector('.page-btn-left')
let pageBtnDoubleLeft = document.querySelector('.page-btn-doubleLeft')
let pageBtnRight = document.querySelector('.page-btn-right')
let pageBtnDoubleRight = document.querySelector('.page-btn-doubleRight')
let p2 = pageNumber[2];
let p4 = pageNumber[4];
let p3 = pageNumber[3];
let p5 = pageNumber[5];
let p1 = pageNumber[1];
let lastPage = pageNumber[pageNumber.length - 1];

//Удаление активной страницы
const removeActivePage = () => {
    let activePages = document.querySelectorAll('.active-page');
    activePages.forEach(el => {
        el.classList.remove('active-page')
    })
}

function fetchPage(page) {
    fetch('', {
        method: 'POST',
        body: page,
    }).then(async res => {
        if (res.ok) {
            fetchSection.style.display = 'flex';
            let result = await res.json();
            cardSection.innerHTML = '';
            result.items.forEach(item => {
                let itemHTML = cardPage(item.name, item.dataType, item.projectName, item.size, item.square, item.howDays, item.price1, item.price2, item.src);
                cardSection.append(itemHTML)
            })
            fetchSection.style.display = 'none';
        } else {
            alert('Что-то пошло не так!');
            console.error(res.code);
            fetchSection.style.display = 'none';
            return false;
        }
    }).catch(err => {
        alert('Что-то пошло не так!');
        console.error(err);
        fetchSection.style.display = 'none';
        return false;
    })
}

function cardPage(name, dataType, projectName, size, square, howDays, price1, price2, src) {
    return `<article class="card">
                <a href="#">
                    <img alt="home1" class="project" src="${src}">
                    <div class="project-name">
                        <h3 class="project-h">${name}</h3>
                        <div class="project-button">
                            <button data-like-id="${dataType}" type="button"
                                    class="project-buttons project-like">
                                <svg class="LikeCard" width="36" height="36" viewBox="0 0 36 36" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M30.5293 8.35757C29.0428 6.83732 27.0545 6.00006 24.9303 6.00006C22.839 6.00006 20.8726 6.8146 19.3934 8.29389L18.6299 9.05782L17.8658 8.29367C16.3867 6.81455 14.4207 6 12.3299 6C10.2385 6 8.27218 6.81455 6.79317 8.29367C5.31438 9.7724 4.5 11.7386 4.5 13.83C4.5 15.9214 5.31438 17.8875 6.79317 19.3663L16.5791 29.1522C17.1444 29.7174 17.8868 30 18.6293 30C19.3717 29.9999 20.1142 29.7174 20.6794 29.1522L30.4023 19.4302C33.4561 16.3763 33.5131 11.4092 30.5293 8.35757ZM29.3771 18.4051L19.6543 28.1272C19.0891 28.6924 18.1694 28.6924 17.6042 28.1271L7.81823 18.3411C6.61326 17.1362 5.94968 15.5341 5.94968 13.8299C5.94968 12.1257 6.61326 10.5237 7.81823 9.31867C9.02343 8.11342 10.6257 7.44968 12.3299 7.44968C14.0334 7.44968 15.6354 8.11342 16.8407 9.31867L17.6052 10.0832L16.4345 11.2548C16.1515 11.538 16.1517 11.9969 16.4349 12.2799C16.5764 12.4213 16.7618 12.492 16.9472 12.492C17.1328 12.492 17.3184 12.4211 17.4599 12.2795L20.4185 9.31862C21.6237 8.11337 23.226 7.44962 24.9302 7.44962C26.6609 7.44962 28.2812 8.132 29.4926 9.371C31.924 11.8575 31.8721 15.9101 29.3771 18.4051Z"
                                          fill="#969BAB"/>
                                    <path d="M30.2099 13.501C30.1303 12.2119 29.5841 10.9997 28.6721 10.0875C28.0263 9.44176 27.225 8.97254 26.3549 8.73032C25.9692 8.62302 25.5696 8.8487 25.4623 9.23439C25.355 9.62002 25.5807 10.0197 25.9664 10.127C26.5964 10.3022 27.1776 10.643 27.6471 11.1125C28.3089 11.7744 28.7053 12.6543 28.7631 13.5903C28.7869 13.9747 29.106 14.2705 29.4859 14.2705C29.5009 14.2705 29.5161 14.27 29.5313 14.2691C29.9308 14.2445 30.2346 13.9006 30.2099 13.501Z"
                                          fill="#969BAB"/>
                                </svg>
                            </button>
                            <button data-share-btn="${dataType}" type="button"
                                    class="project-buttons project-share">
                                <svg width="36" height="36" viewBox="0 0 36 36" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M18.8704 4.35525L23.7434 9.55499C24.0658 9.89772 24.0201 10.4134 23.6375 10.705C23.4691 10.8356 23.2621 10.8987 23.0551 10.8987C22.7989 10.8987 22.545 10.8008 22.3669 10.6104L18.8439 6.85012V21.5375C18.8439 21.9891 18.4397 22.3536 17.9415 22.3536C17.4434 22.3536 17.0391 21.9891 17.0391 21.5375V6.85992L13.5162 10.6191C13.3381 10.8095 13.083 10.9063 12.8279 10.9063C12.621 10.9063 12.414 10.8432 12.2444 10.7127C11.8642 10.4232 11.8172 9.90751 12.1397 9.56369L17.0139 4.36287L17.0331 4.34111C17.1258 4.23448 17.4542 3.85693 17.9415 3.85693C18.4385 3.85693 18.7573 4.2236 18.8512 4.33349L18.8704 4.35525ZM7.71429 29.1506C7.71429 30.8011 9.19784 32.1427 11.0231 32.1427H24.8599C26.6852 32.1427 28.1687 30.8011 28.1687 29.1506V16.6381C28.1687 14.9876 26.6852 13.646 24.8599 13.646H22.4331V15.2781H24.8599C25.6889 15.2781 26.3639 15.8885 26.3639 16.6381V29.1506C26.3639 29.9002 25.6889 30.5106 24.8599 30.5106H11.0231C10.1941 30.5106 9.5191 29.9002 9.5191 29.1506V16.6381C9.5191 15.8885 10.1941 15.2781 11.0231 15.2781H13.5005V13.646H11.0231C9.19784 13.646 7.71429 14.9876 7.71429 16.6381V29.1506Z"
                                          fill="#969BAB"/>
                                </svg>
                            </button>
                            <nav data-share-nav="${dataType}" class="project-link">
                                <button
                                        class="btn-share vkontakte"
                                        onClick="window.open('https://vkontakte.ru/share.php?url=https://only-to-top.ru/blog/coding/2019-11-01-knopki-podelitsya-dlya-sajta.html','sharer','status=0,toolbar=0,width=650,height=500'); return false;"
                                        title="Поделиться Вконтакте">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="48px"
                                         height="48px">
                                        <path fill="#1976d2" d="M24 4A20 20 0 1 0 24 44A20 20 0 1 0 24 4Z"/>
                                        <path fill="#fff"
                                              d="M35.937,18.041c0.046-0.151,0.068-0.291,0.062-0.416C35.984,17.263,35.735,17,35.149,17h-2.618 c-0.661,0-0.966,0.4-1.144,0.801c0,0-1.632,3.359-3.513,5.574c-0.61,0.641-0.92,0.625-1.25,0.625C26.447,24,26,23.786,26,23.199 v-5.185C26,17.32,25.827,17,25.268,17h-4.649C20.212,17,20,17.32,20,17.641c0,0.667,0.898,0.827,1,2.696v3.623 C21,24.84,20.847,25,20.517,25c-0.89,0-2.642-3-3.815-6.932C16.448,17.294,16.194,17,15.533,17h-2.643 C12.127,17,12,17.374,12,17.774c0,0.721,0.6,4.619,3.875,9.101C18.25,30.125,21.379,32,24.149,32c1.678,0,1.85-0.427,1.85-1.094 v-2.972C26,27.133,26.183,27,26.717,27c0.381,0,1.158,0.25,2.658,2c1.73,2.018,2.044,3,3.036,3h2.618 c0.608,0,0.957-0.255,0.971-0.75c0.003-0.126-0.015-0.267-0.056-0.424c-0.194-0.576-1.084-1.984-2.194-3.326 c-0.615-0.743-1.222-1.479-1.501-1.879C32.062,25.36,31.991,25.176,32,25c0.009-0.185,0.105-0.361,0.249-0.607 C32.223,24.393,35.607,19.642,35.937,18.041z"/>
                                    </svg>
                                </button>
                                <button
                                        class="btn-share telegram"
                                        onClick="window.open('https://telegram.me/share/url?url=АДРЕС_СТРАНИЦЫ','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                        title="Поделиться в Телеграм">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="48px"
                                         height="48px">
                                        <linearGradient id="BiF7D16UlC0RZ_VqXJHnXa" x1="9.858" x2="38.142"
                                                        y1="9.858" y2="38.142"
                                                        gradientUnits="userSpaceOnUse">
                                            <stop offset="0" stop-color="#33bef0"/>
                                            <stop offset="1" stop-color="#0a85d9"/>
                                        </linearGradient>
                                        <path fill="url(#BiF7D16UlC0RZ_VqXJHnXa)"
                                              d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/>
                                        <path d="M10.119,23.466c8.155-3.695,17.733-7.704,19.208-8.284c3.252-1.279,4.67,0.028,4.448,2.113\tc-0.273,2.555-1.567,9.99-2.363,15.317c-0.466,3.117-2.154,4.072-4.059,2.863c-1.445-0.917-6.413-4.17-7.72-5.282\tc-0.891-0.758-1.512-1.608-0.88-2.474c0.185-0.253,0.658-0.763,0.921-1.017c1.319-1.278,1.141-1.553-0.454-0.412\tc-0.19,0.136-1.292,0.935-1.745,1.237c-1.11,0.74-2.131,0.78-3.862,0.192c-1.416-0.481-2.776-0.852-3.634-1.223\tC8.794,25.983,8.34,24.272,10.119,23.466z"
                                              opacity=".05"/>
                                        <path d="M10.836,23.591c7.572-3.385,16.884-7.264,18.246-7.813c3.264-1.318,4.465-0.536,4.114,2.011\tc-0.326,2.358-1.483,9.654-2.294,14.545c-0.478,2.879-1.874,3.513-3.692,2.337c-1.139-0.734-5.723-3.754-6.835-4.633\tc-0.86-0.679-1.751-1.463-0.71-2.598c0.348-0.379,2.27-2.234,3.707-3.614c0.833-0.801,0.536-1.196-0.469-0.508\tc-1.843,1.263-4.858,3.262-5.396,3.625c-1.025,0.69-1.988,0.856-3.664,0.329c-1.321-0.416-2.597-0.819-3.262-1.078\tC9.095,25.618,9.075,24.378,10.836,23.591z"
                                              opacity=".07"/>
                                        <path fill="#fff"
                                              d="M11.553,23.717c6.99-3.075,16.035-6.824,17.284-7.343c3.275-1.358,4.28-1.098,3.779,1.91\tc-0.36,2.162-1.398,9.319-2.226,13.774c-0.491,2.642-1.593,2.955-3.325,1.812c-0.833-0.55-5.038-3.331-5.951-3.984\tc-0.833-0.595-1.982-1.311-0.541-2.721c0.513-0.502,3.874-3.712,6.493-6.21c0.343-0.328-0.088-0.867-0.484-0.604\tc-3.53,2.341-8.424,5.59-9.047,6.013c-0.941,0.639-1.845,0.932-3.467,0.466c-1.226-0.352-2.423-0.772-2.889-0.932\tC9.384,25.282,9.81,24.484,11.553,23.717z"/>
                                    </svg>
                                </button>
                                <button
                                        class="btn-share whatsapp"
                                        onClick="window.open('whatsapp://send?text=ТАЙТЛ_СТРАНИЦЫ. ОПИСАНИЕ. СССЫЛКА','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                        title="Поделиться в WhatsApp">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="40px"
                                         height="40px">
                                        <path fill="#f2faff"
                                              d="M4.221,29.298l-0.104-0.181c-1.608-2.786-2.459-5.969-2.458-9.205 C1.663,9.76,9.926,1.5,20.078,1.5c4.926,0.002,9.553,1.919,13.03,5.399c3.477,3.48,5.392,8.107,5.392,13.028 c-0.005,10.153-8.268,18.414-18.42,18.414c-3.082-0.002-6.126-0.776-8.811-2.24l-0.174-0.096l-9.385,2.46L4.221,29.298z"/>
                                        <path fill="#788b9c"
                                              d="M20.078,2L20.078,2c4.791,0.001,9.293,1.867,12.676,5.253C36.137,10.639,38,15.14,38,19.927 c-0.005,9.878-8.043,17.914-17.927,17.914c-2.991-0.001-5.952-0.755-8.564-2.18l-0.349-0.19l-0.384,0.101l-8.354,2.19 l2.226-8.131l0.11-0.403L4.55,28.867c-1.566-2.711-2.393-5.808-2.391-8.955C2.163,10.036,10.202,2,20.078,2 M20.078,1 C9.651,1,1.163,9.485,1.158,19.912c-0.002,3.333,0.869,6.588,2.525,9.455L1,39.169l10.03-2.63c2.763,1.507,5.875,2.3,9.042,2.302 h0.008c10.427,0,18.915-8.485,18.92-18.914c0-5.054-1.966-9.807-5.538-13.382C29.89,2.971,25.14,1.002,20.078,1L20.078,1z"/>
                                        <path fill="#79ba7e"
                                              d="M19.995,35c-2.504-0.001-4.982-0.632-7.166-1.823l-1.433-0.782l-1.579,0.414l-3.241,0.85l0.83-3.03\tl0.453-1.656L7,27.485c-1.309-2.267-2.001-4.858-2-7.492C5.004,11.726,11.732,5.001,19.998,5c4.011,0.001,7.779,1.563,10.61,4.397\tC33.441,12.231,35,15.999,35,20.005C34.996,28.273,28.268,35,19.995,35z"/>
                                        <path fill="#fff"
                                              d="M28.28,23.688c-0.45-0.224-2.66-1.313-3.071-1.462c-0.413-0.151-0.712-0.224-1.012,0.224\tc-0.3,0.45-1.161,1.462-1.423,1.761c-0.262,0.3-0.524,0.337-0.974,0.113c-0.45-0.224-1.899-0.7-3.615-2.231\tc-1.337-1.191-2.239-2.663-2.501-3.113c-0.262-0.45-0.029-0.693,0.197-0.917c0.202-0.202,0.45-0.525,0.674-0.787\tc0.224-0.262,0.3-0.45,0.45-0.75c0.151-0.3,0.075-0.563-0.038-0.787c-0.113-0.224-1.012-2.437-1.387-3.336\tc-0.364-0.876-0.736-0.757-1.012-0.771c-0.262-0.014-0.562-0.015-0.861-0.015c-0.3,0-0.787,0.113-1.198,0.563\tc-0.411,0.45-1.573,1.537-1.573,3.749s1.611,4.35,1.835,4.649c0.224,0.3,3.169,4.839,7.68,6.786\tc1.072,0.462,1.911,0.739,2.562,0.947c1.076,0.342,2.057,0.294,2.832,0.178c0.864-0.129,2.66-1.087,3.034-2.136\tc0.375-1.049,0.375-1.95,0.262-2.136C29.03,24.025,28.731,23.912,28.28,23.688z"/>
                                    </svg>
                                </button>
                                <button
                                        class="btn-share viber"
                                        onClick="window.open('viber://forward?text=GlavStroy365   ОПИСАНИЕ. СССЫЛКА','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                        title="Поделиться в Viber">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="48px"
                                         height="48px">
                                        <path fill="#fff"
                                              d="M24,5C21.361,5,13.33,5,8.89,9.054C6.246,11.688,5,15.494,5,21v3c0,5.506,1.246,9.312,3.921,11.976 c1.332,1.215,3.148,2.186,5.368,2.857L15,39.047v5.328C15,45,15.181,45,15.241,45c0.123,0,0.32-0.039,0.694-0.371 c0.09-0.089,0.75-0.803,3.96-4.399l0.324-0.363l0.485,0.031C21.779,39.965,22.888,40,24,40c2.639,0,10.67,0,15.11-4.055 C41.753,33.311,43,29.505,43,24v-3c0-5.506-1.246-9.312-3.921-11.976C34.67,5,26.639,5,24,5z"/>
                                        <path fill="#7e57c2"
                                              d="M33.451 28.854c-1.111-.936-1.624-1.219-3.158-2.14C29.654 26.331 28.68 26 28.169 26c-.349 0-.767.267-1.023.523C26.49 27.179 26.275 28 25.125 28c-1.125 0-3.09-1.145-4.5-2.625C19.145 23.965 18 22 18 20.875c0-1.15.806-1.38 1.462-2.037C19.718 18.583 20 18.165 20 17.816c0-.511-.331-1.47-.714-2.109-.921-1.535-1.203-2.048-2.14-3.158-.317-.376-.678-.548-1.056-.549-.639-.001-1.478.316-2.046.739-.854.637-1.747 1.504-1.986 2.584-.032.147-.051.295-.057.443-.046 1.125.396 2.267.873 3.234 1.123 2.279 2.609 4.485 4.226 6.455.517.63 1.08 1.216 1.663 1.782.566.582 1.152 1.145 1.782 1.663 1.97 1.617 4.176 3.103 6.455 4.226.958.472 2.086.906 3.2.874.159-.005.318-.023.477-.058 1.08-.238 1.947-1.132 2.584-1.986.423-.568.74-1.406.739-2.046C33.999 29.532 33.827 29.171 33.451 28.854zM34 24c-.552 0-1-.448-1-1v-1c0-4.962-4.038-9-9-9-.552 0-1-.448-1-1s.448-1 1-1c6.065 0 11 4.935 11 11v1C35 23.552 34.552 24 34 24zM27.858 22c-.444 0-.85-.298-.967-.748-.274-1.051-1.094-1.872-2.141-2.142-.535-.139-.856-.684-.718-1.219.138-.534.682-.855 1.219-.718 1.748.453 3.118 1.822 3.575 3.574.139.535-.181 1.08-.715 1.22C28.026 21.989 27.941 22 27.858 22z"/>
                                        <path fill="#7e57c2"
                                              d="M31,23c-0.552,0-1-0.448-1-1c0-3.188-2.494-5.818-5.678-5.986c-0.552-0.029-0.975-0.5-0.946-1.051 c0.029-0.552,0.508-0.976,1.051-0.946C28.674,14.241,32,17.748,32,22C32,22.552,31.552,23,31,23z"/>
                                        <path fill="#7e57c2"
                                              d="M24,4C19.5,4,12.488,4.414,8.216,8.316C5.196,11.323,4,15.541,4,21c0,0.452-0.002,0.956,0.002,1.5 C3.998,23.043,4,23.547,4,23.999c0,5.459,1.196,9.677,4.216,12.684c1.626,1.485,3.654,2.462,5.784,3.106v4.586 C14,45.971,15.049,46,15.241,46h0.009c0.494-0.002,0.921-0.244,1.349-0.624c0.161-0.143,2.02-2.215,4.042-4.481 C21.845,40.972,22.989,41,23.999,41c0,0,0,0,0,0s0,0,0,0c4.5,0,11.511-0.415,15.784-4.317c3.019-3.006,4.216-7.225,4.216-12.684 c0-0.452,0.002-0.956-0.002-1.5c0.004-0.544,0.002-1.047,0.002-1.5c0-5.459-1.196-9.677-4.216-12.684C35.511,4.414,28.5,4,24,4z M41,23.651l0,0.348c0,4.906-1.045,8.249-3.286,10.512C33.832,38,26.437,38,23.999,38c-0.742,0-1.946-0.001-3.367-0.1 C20.237,38.344,16,43.083,16,43.083V37.22c-2.104-0.505-4.183-1.333-5.714-2.708C8.045,32.248,7,28.905,7,23.999l0-0.348 c0-0.351-0.001-0.73,0.002-1.173C6.999,22.078,6.999,21.7,7,21.348L7,21c0-4.906,1.045-8.249,3.286-10.512 C14.167,6.999,21.563,6.999,24,6.999c2.437,0,9.832,0,13.713,3.489c2.242,2.263,3.286,5.606,3.286,10.512l0,0.348 c0,0.351,0.001,0.73-0.002,1.173C41,22.922,41,23.3,41,23.651z"/>
                                    </svg>
                                </button>
                            </nav>
                        </div>
                    </div>
                    <p class="project-nametxt">${projectName}</p>
                    <div class="card-footer">
                        <div class="project-info">
                            <div class="project-infoRow">
                                <svg width="36" height="36" viewBox="0 0 36 36" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M34.4462 31.1782H31.5342V14.6232L33.1727 15.7132C33.7755 16.1141 34.5542 16.1141 35.1569 15.7132C35.6572 15.3804 35.9559 14.8229 35.9559 14.2218C35.9559 13.6208 35.6572 13.0634 35.1569 12.7305L31.2225 10.1133C30.9869 9.95646 30.6689 10.0205 30.5122 10.2561C30.3555 10.4917 30.4194 10.8097 30.655 10.9664L34.5893 13.5836C34.8067 13.7281 34.9312 13.9607 34.9312 14.2218C34.9312 14.4828 34.8067 14.7154 34.5893 14.86C34.3314 15.0316 33.9982 15.0315 33.7403 14.86L18.5038 4.7245C18.1977 4.52102 17.8022 4.52095 17.4961 4.72465L2.2597 14.8601C2.0018 15.0316 1.66852 15.0317 1.41061 14.86C1.19327 14.7155 1.06868 14.4829 1.06868 14.2218C1.06868 13.9608 1.19334 13.7282 1.41061 13.5836L18 2.54826L28.0052 9.20383C28.2407 9.36056 28.5588 9.29658 28.7155 9.0611C28.8723 8.82548 28.8083 8.50746 28.5728 8.3508L18.5038 1.65262C18.1977 1.44914 17.8022 1.44907 17.4961 1.65276L12.5184 4.96399V4.52032H12.6663C13.0334 4.52032 13.332 4.22163 13.332 3.8546V2.30027C13.332 1.93324 13.0333 1.63455 12.6663 1.63455H7.24634C6.87923 1.63455 6.58062 1.93324 6.58062 2.30027V3.85467C6.58062 4.2217 6.87923 4.52039 7.24634 4.52039H7.39427V8.3726L0.843117 12.7305C0.342703 13.0634 0.0440156 13.6209 0.0440156 14.2219C0.0440156 14.8229 0.342773 15.3804 0.843117 15.7132C1.14448 15.9138 1.48978 16.014 1.83523 16.014C2.18053 16.014 2.52598 15.9137 2.82727 15.7132L4.46576 14.6233V23.7586C4.46576 24.0416 4.69512 24.271 4.97813 24.271C5.26106 24.271 5.49049 24.0416 5.49049 23.7586V13.9416L18 5.62008L30.5095 13.9416V31.1782H5.49042V26.9417C5.49042 26.6587 5.26106 26.4294 4.97806 26.4294C4.69512 26.4294 4.46569 26.6588 4.46569 26.9417V31.1782H1.55377C0.697008 31.1782 0 31.8753 0 32.732C0 33.5888 0.697008 34.2858 1.55377 34.2858H34.4462C35.303 34.2858 36 33.5888 36 32.732C36 31.8753 35.303 31.1782 34.4462 31.1782ZM11.4937 5.64567L8.41894 7.69099V4.52039H11.4937V5.64567ZM7.60535 2.65922H12.3074V3.49572H7.60535V2.65922ZM34.4462 33.2611H1.55377C1.26204 33.2611 1.02466 33.0237 1.02466 32.732C1.02466 32.4402 1.26204 32.2028 1.55377 32.2028H34.4462C34.738 32.2028 34.9753 32.4402 34.9753 32.732C34.9753 33.0237 34.738 33.2611 34.4462 33.2611Z"
                                          fill="#474A57"/>
                                    <g clip-path="url(#clip0_1005_1804)">
                                        <path d="M9 29.0002L15.3273 22.9902" stroke="#474A57"
                                              stroke-width="0.7"/>
                                        <path d="M9.56927 28.4558L15.3272 22.9902" stroke="#474A57"
                                              stroke-width="0.7" stroke-miterlimit="10"/>
                                        <path d="M9.37548 28.6501L9.63443 24.9678L9.65708 24.9695L9.42914 28.2401L9.403 28.6152L9.77902 28.6145L12.9811 28.6078V28.6501H9.37548Z"
                                              fill="black" stroke="#474A57" stroke-width="0.7"/>
                                        <path d="M26.0001 29.0002L19.6729 22.9902" stroke="#474A57"
                                              stroke-width="0.7"/>
                                        <path d="M25.4236 28.4558L19.6729 22.9902" stroke="#474A57"
                                              stroke-width="0.7" stroke-miterlimit="10"/>
                                        <path d="M25.6241 28.6494L22.0192 28.6433V28.6078L25.2212 28.6145L25.598 28.6152L25.5711 28.2394L25.3367 24.9694L25.3592 24.9678L25.6241 28.6494Z"
                                              fill="black" stroke="#474A57" stroke-width="0.7"/>
                                        <path d="M26.0001 12L19.6729 18.01" stroke="#474A57"
                                              stroke-width="0.7"/>
                                        <path d="M25.4236 12.5444L19.6729 18.01" stroke="#474A57"
                                              stroke-width="0.7"
                                              stroke-miterlimit="10"/>
                                        <path d="M25.3586 16.0322L25.3367 16.0307L25.5711 12.7606L25.598 12.3856H25.222H22.0192V12.35H25.6176L25.3586 16.0322Z"
                                              fill="black" stroke="#474A57" stroke-width="0.7"/>
                                        <path d="M9 12L15.3273 18.01" stroke="#474A57" stroke-width="0.7"/>
                                        <path d="M9.56927 12.5444L15.3272 18.01" stroke="#474A57"
                                              stroke-width="0.7"
                                              stroke-miterlimit="10"/>
                                        <path d="M9.42199 12.7606L9.65639 16.0307L9.63443 16.0322L9.37548 12.35H12.9811V12.3922L9.77181 12.3856L9.39505 12.3848L9.42199 12.7606Z"
                                              fill="black" stroke="#474A57" stroke-width="0.7"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_1005_1804">
                                            <rect width="17" height="17" fill="white"
                                                  transform="translate(9 12)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                <div class="project-size">Размеры</div>
                                <div class="project-txtSize">${size} м&sup2;</div>
                            </div>
                            <div class="project-infoRow">
                                <svg width="36" height="36" viewBox="0 0 36 36" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M34.4462 31.1782H31.5342V14.6232L33.1727 15.7132C33.7755 16.1141 34.5542 16.1141 35.1569 15.7132C35.6572 15.3804 35.9559 14.8229 35.9559 14.2218C35.9559 13.6208 35.6572 13.0634 35.1569 12.7305L31.2225 10.1133C30.9869 9.95646 30.6689 10.0205 30.5122 10.2561C30.3555 10.4917 30.4194 10.8097 30.655 10.9664L34.5893 13.5836C34.8067 13.7281 34.9312 13.9607 34.9312 14.2218C34.9312 14.4828 34.8067 14.7154 34.5893 14.86C34.3314 15.0316 33.9982 15.0315 33.7403 14.86L18.5038 4.7245C18.1977 4.52102 17.8022 4.52095 17.4961 4.72465L2.2597 14.8601C2.0018 15.0316 1.66852 15.0317 1.41061 14.86C1.19327 14.7155 1.06868 14.4829 1.06868 14.2218C1.06868 13.9608 1.19334 13.7282 1.41061 13.5836L18 2.54826L28.0052 9.20383C28.2407 9.36056 28.5588 9.29658 28.7155 9.0611C28.8723 8.82548 28.8083 8.50746 28.5728 8.3508L18.5038 1.65262C18.1977 1.44914 17.8022 1.44907 17.4961 1.65276L12.5184 4.96399V4.52032H12.6663C13.0334 4.52032 13.332 4.22163 13.332 3.8546V2.30027C13.332 1.93324 13.0333 1.63455 12.6663 1.63455H7.24634C6.87923 1.63455 6.58062 1.93324 6.58062 2.30027V3.85467C6.58062 4.2217 6.87923 4.52039 7.24634 4.52039H7.39427V8.3726L0.843117 12.7305C0.342703 13.0634 0.0440156 13.6209 0.0440156 14.2219C0.0440156 14.8229 0.342773 15.3804 0.843117 15.7132C1.14448 15.9138 1.48978 16.014 1.83523 16.014C2.18053 16.014 2.52598 15.9137 2.82727 15.7132L4.46576 14.6233V23.7586C4.46576 24.0416 4.69512 24.271 4.97813 24.271C5.26106 24.271 5.49049 24.0416 5.49049 23.7586V13.9416L18 5.62008L30.5095 13.9416V31.1782H5.49042V26.9417C5.49042 26.6587 5.26106 26.4294 4.97806 26.4294C4.69512 26.4294 4.46569 26.6588 4.46569 26.9417V31.1782H1.55377C0.697008 31.1782 0 31.8753 0 32.732C0 33.5888 0.697008 34.2858 1.55377 34.2858H34.4462C35.303 34.2858 36 33.5888 36 32.732C36 31.8753 35.303 31.1782 34.4462 31.1782ZM11.4937 5.64567L8.41894 7.69099V4.52039H11.4937V5.64567ZM7.60535 2.65922H12.3074V3.49572H7.60535V2.65922ZM34.4462 33.2611H1.55377C1.26204 33.2611 1.02466 33.0237 1.02466 32.732C1.02466 32.4402 1.26204 32.2028 1.55377 32.2028H34.4462C34.738 32.2028 34.9753 32.4402 34.9753 32.732C34.9753 33.0237 34.738 33.2611 34.4462 33.2611Z"
                                          fill="#474A57"/>
                                    <path d="M7.229 27.8647L28.7237 16.0645" stroke="#474A57"
                                          stroke-width="0.7"
                                          stroke-miterlimit="10"/>
                                    <path d="M10.309 29.0802L10.4699 28.4732L7.44722 27.7451L8.45551 24.804L7.85704 24.6138L6.62651 28.1956L10.309 29.0802Z"
                                          fill="#474A57"/>
                                    <path d="M28.1025 19.3121L27.4984 19.125L28.5123 16.1808L25.4841 15.4557L25.6505 14.8456L29.3274 15.7333L28.1025 19.3121Z"
                                          fill="#474A57"/>
                                </svg>
                                <div class="project-size">Площадь</div>
                                <div class="project-txtSize">${square}м&sup2;</div>
                            </div>
                            <div class="project-infoRow">
                                <svg width="169" height="169" viewBox="0 0 169 169" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M161.706 146.365H148.035V68.648L155.728 73.7649C158.557 75.6473 162.213 75.647 165.042 73.7652C167.391 72.2026 168.793 69.5854 168.793 66.7639C168.793 63.9424 167.391 61.3255 165.042 59.7629L146.573 47.4768C145.466 46.7404 143.973 47.0411 143.238 48.1468C142.502 49.2529 142.802 50.7459 143.908 51.4813L162.378 63.7678C163.398 64.4461 163.983 65.538 163.983 66.7636C163.983 67.9891 163.398 69.081 162.378 69.7597C161.167 70.5654 159.603 70.5651 158.392 69.7597L86.865 22.1793C85.4282 21.224 83.5715 21.2237 82.1347 22.1799L10.6081 69.76C9.39732 70.5654 7.83275 70.5657 6.62203 69.7597C5.60176 69.0814 5.01686 67.9895 5.01686 66.7639C5.01686 65.5383 5.60209 64.4464 6.62203 63.7678L84.5 11.963L131.469 43.2072C132.575 43.943 134.068 43.6426 134.803 42.5371C135.539 41.4311 135.239 39.9381 134.133 39.2027L86.865 7.75847C85.4282 6.80322 83.5715 6.80289 82.1347 7.75913L58.7668 23.3035V21.2207H59.4613C61.1846 21.2207 62.5864 19.8185 62.5864 18.0955V10.7988C62.5864 9.07581 61.1843 7.67364 59.4613 7.67364H34.0175C32.2942 7.67364 30.8923 9.07581 30.8923 10.7988V18.0959C30.8923 19.8189 32.2942 21.221 34.0175 21.221H34.712V39.305L3.95797 59.7629C1.6088 61.3255 0.206629 63.9427 0.206629 66.7642C0.206629 69.5857 1.60913 72.2026 3.95797 73.7652C5.37268 74.7066 6.9937 75.1769 8.61537 75.1769C10.2364 75.1769 11.8581 74.7062 13.2724 73.7652L20.9643 68.6486V111.534C20.9643 112.862 22.041 113.939 23.3695 113.939C24.6978 113.939 25.7748 112.862 25.7748 111.534V65.4482L84.5 26.3835L143.225 65.4482V146.365H25.7745V126.477C25.7745 125.148 24.6978 124.072 23.3692 124.072C22.041 124.072 20.9639 125.148 20.9639 126.477V146.365H7.29407C3.27206 146.365 0 149.637 0 153.659C0 157.681 3.27206 160.953 7.29407 160.953H161.706C165.728 160.953 169 157.681 169 153.659C169 149.637 165.728 146.365 161.706 146.365ZM53.9566 26.5036L39.5222 36.1052V21.221H53.9566V26.5036ZM35.7029 12.4839H57.7765V16.4108H35.7029V12.4839ZM161.706 156.143H7.29407C5.92457 156.143 4.81023 155.028 4.81023 153.659C4.81023 152.289 5.92457 151.175 7.29407 151.175H161.706C163.075 151.175 164.19 152.289 164.19 153.659C164.19 155.028 163.075 156.143 161.706 156.143Z"
                                          fill="#474A57"/>
                                    <path d="M85.6594 70.5681V63.2696C102.522 63.907 115.705 78.0668 115.123 94.9448C114.539 111.857 100.356 125.095 83.4437 124.511C66.5311 123.928 53.2937 109.745 53.8769 92.8326C54.1444 85.0763 57.3452 77.7104 62.8329 72.2227L62.8342 72.2214C63.2792 71.7606 63.2664 71.0262 62.8056 70.5811C62.3561 70.147 61.6434 70.147 61.1939 70.5811L61.1926 70.5824C48.3207 83.4546 48.3211 104.324 61.1933 117.196C74.0655 130.068 94.9354 130.068 107.807 117.196C120.679 104.323 120.679 83.4535 107.807 70.5816C101.625 64.4002 93.2412 60.9277 84.4993 60.9278C83.8587 60.9278 83.3393 61.4472 83.3393 62.0879V70.5681C83.3393 71.2088 83.8587 71.7282 84.4993 71.7282C85.14 71.7282 85.6594 71.2088 85.6594 70.5681Z"
                                          fill="black" stroke="#303D4E" stroke-width="0.2"/>
                                    <path d="M85.5096 89.8809L85.5092 89.8807C84.7103 89.5781 83.8465 89.4905 83.004 89.6262L73.1196 67.3967L73.1196 67.3967L73.1189 67.3951C72.8131 66.7386 72.0329 66.4541 71.3764 66.7599C70.7359 67.0581 70.4468 67.8102 70.7226 68.4607L70.7226 68.4607L70.7233 68.4623L80.6077 90.6917C79.9427 91.2264 79.4291 91.9264 79.1186 92.7223L79.1184 92.7228C78.1383 95.2724 79.4107 98.1338 81.9603 99.1139C84.5099 100.094 87.3714 98.8217 88.3515 96.2721C89.3316 93.7225 88.0592 90.861 85.5096 89.8809ZM82.9015 96.6657C81.7039 96.2053 81.1064 94.8615 81.5667 93.6639C82.0271 92.4664 83.3709 91.8688 84.5685 92.3292C85.766 92.7895 86.3636 94.1334 85.9032 95.3309C85.4429 96.5285 84.099 97.126 82.9015 96.6657Z"
                                          fill="black" stroke="#18191F" stroke-width="0.2"/>
                                </svg>
                                <div class="project-size">Сроки строительства</div>
                                <div class="project-txtSize">${howDays} дней</div>
                            </div>
                        </div>
                        <span class="project-border"></span>
                        <div class="project-footer">
                            <div class="project-footerInfo">
                                <div class="project-type">Без Отделки</div>
                                <div class="project-summ">${price1} ₽</div>
                            </div>
                            <div class="project-footerInfo">
                                <div class="project-type">«Под ключ»</div>
                                <div class="project-summ">${price2} ₽</div>
                            </div>
                        </div>
                    </div>
                </a>
            </article>`;
}

pageNumber.forEach(el => {
    el.textContent = el.dataset.pageCardNumber
    el.addEventListener('click', () => {
        let needPage = el.dataset.pageCardNumber
        let currentPage = document.querySelector('.active-page');
        page = +needPage + 1;
        if (needPage === currentPage.dataset.pageCardNumber || needPage === '...') {
            return false;
        } else {
            fetchPage(needPage);
            cardSection.dataset.pageCard = needPage;
            if (needPage >= 4) {
                removeActivePage()
                p3.classList.add('active-page');
                if (needPage >= 5) {
                    p1.dataset.pageCardNumber = '...'
                    if (needPage >= lastPage.dataset.pageCardNumber - 3) {
                        if (needPage >= lastPage.dataset.pageCardNumber - 2) {
                            p5.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 1) + '';
                            removeActivePage()
                            el.classList.toggle('active-page');
                            if (needPage === lastPage.dataset.pageCardNumber) {
                                removeActivePage();
                                lastPage.classList.add('active-page')
                                p5.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 1) + '';
                                p4.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 2) + '';
                                p3.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 3) + '';
                                p2.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 4) + '';
                            }
                        } else {
                            p5.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 1) + '';
                            p4.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 2) + '';
                            p3.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 3) + '';
                            p2.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 4) + '';
                        }
                    } else {
                        p5.dataset.pageCardNumber = '...';
                        p1.dataset.pageCardNumber = '...';
                        p3.dataset.pageCardNumber = needPage
                        p2.dataset.pageCardNumber = (+needPage - 1) + '';
                        p4.dataset.pageCardNumber = (+needPage + 1) + '';
                    }
                } else {
                    p3.dataset.pageCardNumber = needPage
                    p2.dataset.pageCardNumber = (+needPage - 1) + '';
                    p4.dataset.pageCardNumber = (+needPage + 1) + '';
                    p1.dataset.pageCardNumber = '2';
                }
            } else {
                p1.dataset.pageCardNumber = '2';
                removeActivePage()
                el.classList.toggle('active-page')
                if (needPage = 1) {
                    p1.dataset.pageCardNumber = '2';
                    p2.dataset.pageCardNumber = '3';
                    p3.dataset.pageCardNumber = '4';
                    p4.dataset.pageCardNumber = '5';
                    p5.dataset.pageCardNumber = '...';
                }
            }
        }
        if (currentPage)
        pageNumber.forEach(e => {
            e.textContent = e.dataset.pageCardNumber;
        })

        currentPage = document.querySelector('.active-page');
        if (+currentPage.dataset.pageCardNumber === 1) {
            pageBtnLeft.disabled = true;
            pageBtnDoubleLeft.disabled = true;
            pageBtnRight.disabled = false;
            pageBtnDoubleRight.disabled = false;
        } else if (+currentPage.dataset.pageCardNumber === +lastPage.dataset.pageCardNumber) {
            pageBtnRight.disabled = true;
            pageBtnDoubleRight.disabled = true;
            pageBtnLeft.disabled = false;
            pageBtnDoubleLeft.disabled = false;
        } else {
            pageBtnLeft.disabled = false;
            pageBtnDoubleLeft.disabled = false;
            pageBtnRight.disabled = false;
            pageBtnDoubleRight.disabled = false;
        }

    })
})
//Кнопка вправо
pageBtnRight.onclick = function () {

    let currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    let needestPage = +currentPage + 1;
    let needPage = document.querySelector(`[data-page-card-number='${needestPage}']`);
    let needPageData = needPage.dataset.pageCardNumber;
    fetchPage(needestPage)
    if (+lastPage.dataset.pageCardNumber - 3 <= needestPage) {
        p5.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 1) + ''
        p4.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 2) + '';
        p3.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 3) + '';
        if (+lastPage.dataset.pageCardNumber - 3 < needestPage) {
            removeActivePage();
            needPage.classList.toggle('active-page')
        }
    } else  if (needPageData >= 4) {
            removeActivePage()
            p3.classList.add('active-page');
            if (needPageData > 4) {
                p1.dataset.pageCardNumber = '...'
                p3.dataset.pageCardNumber = needestPage;
                p2.dataset.pageCardNumber = (needestPage - 1) + '';
                p4.dataset.pageCardNumber = (needestPage + 1) + '';
            } else {
                p3.dataset.pageCardNumber = needestPage;
                p2.dataset.pageCardNumber = (needestPage - 1) + '';
                p4.dataset.pageCardNumber = (needestPage + 1) + '';
                p1.dataset.pageCardNumber = '2';
            }
    } else {
        removeActivePage();
        needPage.classList.toggle('active-page')
    }
    pageNumber.forEach(e => {
        e.textContent = e.dataset.pageCardNumber;
    })
    currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    if (currentPage === lastPage.dataset.pageCardNumber) {
        pageBtnRight.disabled = true;
        pageBtnDoubleRight.disabled = true;
    }
    pageBtnLeft.disabled = false;
    pageBtnDoubleLeft.disabled = false;
}
pageBtnDoubleRight.onclick = function () {
    removeActivePage();
    lastPage.classList.toggle('active-page')
    let currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    fetchPage(currentPage);
    p5.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 1) + ''
    p4.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 2) + '';
    p3.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 3) + '';
    p2.dataset.pageCardNumber = (+lastPage.dataset.pageCardNumber - 4) + '';
    p1.dataset.pageCardNumber = '...';
    pageBtnRight.disabled = true;
    pageBtnDoubleRight.disabled = true;
    pageBtnLeft.disabled = false;
    pageBtnDoubleLeft.disabled = false;
    pageNumber.forEach(e => {
        e.textContent = e.dataset.pageCardNumber;
    })
}

//кнопки влево
pageBtnLeft.onclick = function () {
    let firstPage = pageNumber[0];
    let currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    let needestPage = +currentPage - 1;
    let needPage = document.querySelector(`[data-page-card-number='${needestPage}']`);
    fetchPage(needestPage)
   if (+lastPage.dataset.pageCardNumber - 3 <= needestPage) {
       removeActivePage();
       needPage.classList.toggle('active-page')

   } else if (+firstPage.dataset.pageCardNumber + 3 >= needestPage ) {
            p1.dataset.pageCardNumber = '2';
            p2.dataset.pageCardNumber = '3'
            p3.dataset.pageCardNumber = '4'
       if (+firstPage.dataset.pageCardNumber + 2 >= needestPage) {
           removeActivePage();
           needPage.classList.toggle('active-page')
       }
   } else {
       removeActivePage();
       p3.classList.add('active-page')
       p3.dataset.pageCardNumber = needestPage;
       p2.dataset.pageCardNumber = (+needestPage -1 ) + '';
       p4.dataset.pageCardNumber = (+needestPage + 1 ) + '';
       p5.dataset.pageCardNumber = '...'
   }
    pageNumber.forEach(e => {
        e.textContent = e.dataset.pageCardNumber;
    })
    currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    if (currentPage === firstPage.dataset.pageCardNumber) {
        pageBtnLeft.disabled = true;
        pageBtnDoubleLeft.disabled = true;
    }
    pageBtnRight.disabled = false;
    pageBtnDoubleRight.disabled = false;
}
pageBtnDoubleLeft.onclick = function () {
    let firstPage = pageNumber[0];
    removeActivePage();
    firstPage.classList.toggle('active-page')
    let currentPage = document.querySelector('.active-page').dataset.pageCardNumber;
    fetchPage(currentPage);
    p1.dataset.pageCardNumber = (+firstPage.dataset.pageCardNumber + 1) + ''
    p2.dataset.pageCardNumber = (+firstPage.dataset.pageCardNumber + 2) + '';
    p3.dataset.pageCardNumber = (+firstPage.dataset.pageCardNumber + 3) + '';
    p4.dataset.pageCardNumber = (+firstPage.dataset.pageCardNumber + 4) + '';
    p5.dataset.pageCardNumber = '...';
    pageBtnLeft.disabled = true;
    pageBtnDoubleLeft.disabled = true;
    pageBtnRight.disabled = false;
    pageBtnDoubleRight.disabled = false;
    pageNumber.forEach(e => {
        e.textContent = e.dataset.pageCardNumber;
    })
}
//Показать еще2
let watchMoreBtn2 = document.querySelector('.watch-more-card');

function lookMore2() {//ToDo Поправит функцию!
    let pageNumber2 = new FormData();
    pageNumber2.append('page', page);

    fetch('', {
        method: "POST",
        body: pageNumber,
    }).then(async res => {
        if (res.ok) {
            let result = await res.json();
            result.items.forEach(item => {
                    //Проверка
                    let itemHTML = cardPage(item.name, item.dataType, item.projectName, item.size, item.square, item.howDays, item.price1, item.price2, item.src); //ToDo Доделать!
                    document.querySelector('.project-block').insertAdjacentHTML('beforeend', itemHTML)
                    if (result.isEnd) {
                        watchMoreBtn2.remove();
                    } else {
                        page++;
                        setTimeout(function () {
                            btnWork = true;
                        }, 500)
                    }
                }
            )
            firstProject();
        } else {
            alert('Что-то пошло не так!');
            console.error(res.code);
        }
    }).catch(err => {
        alert('Произошла непредвиденная ошибка')
        console.log(err);
    });
}

watchMoreBtn2.onclick = lookMore2;