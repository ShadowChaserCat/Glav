const ready = () => {
    let loader = document.getElementById('preloader');
    loader.classList.add('done')
    setTimeout(() => loader.remove(), 500);


    //Slice
    let pSlice = document.querySelectorAll('[data-slice]');
    pSlice.forEach(el => {
       el.textContent = el.textContent.slice(0,300) + '...';
    })


//Меню
    let backgroundMenu = document.querySelector('.phone-background');
//Телефонное меню
    let phoneNumber = document.querySelector('.dropPhone2')
    let navbarLinkPhone = document.querySelectorAll('.navbar-link-phone');
    let navbarMenuPhone = document.querySelectorAll('.navbar-menu-phone')

//поделиться
    let shareBtn = document.querySelectorAll('.project-share');

    let projectList = document.querySelectorAll('.project-link')

//Поиск по сайту
    let searchBtn = document.querySelectorAll('.search-button');
    let searchInp = document.querySelector('.searchInp');

//выпадающее меню
    let DropButton = document.getElementById('DropButton');
    let DropDown = document.querySelector('.drop')
    let phoneMenu = document.querySelector('.phone-header');

    let Burger1 = document.querySelector('.burger1');
    let Burger2 = document.querySelector('.burger2');
    let Burger3 = document.querySelector('.burger3');

//номера
    let open = document.querySelectorAll('.open');
    let dropPhone = document.querySelector('.dropPhone');
    let dataDropPhone = document.querySelector('[data-dropPhone]')

//скролл
    let scrollUp = document.getElementById('up')

//деление на разряды

    document.addEventListener('click', (el) => {
        //Главное меню
        if (DropDown.classList.contains('DropDown')) {
            if (el.target.classList.contains('DropDown') || el.target.parentElement.classList.contains('DropDown') || el.target.parentElement.classList.contains('navbar') || el.target.parentElement.id === 'DropButton' || el.target.parentElement.hasAttribute('data-dropsvg') || el.target.parentElement.classList.contains('navbar-menu')) {
                return false;
            } else {
                dropMenu();
            }
        }

        //номер
        if (dropPhone.classList.contains('dropDownPhone')) {
            if (el.target.classList.contains('dropDownPhone') || el.target.classList.contains('phoneNumbers') || el.target.classList.contains('open') || el.target.hasAttribute('data-dropphone') || el.target.parentElement.hasAttribute('data-dropphone')) {
                return false;
            } else { numberDrop() }
        }

        //Поиск
        if (searchInp.classList.contains('searchInpWidth')) {
            if (el.target.classList.contains('searchInpWidth') ||el.target.classList.contains('searchHidden') || el.target.parentElement.hasAttribute('data-dropsearch') || el.target.parentElement.hasAttribute('data-searchsvg')){
                return false;
            } else {
                btnSearch()
            }
        }

        projectList.forEach(e => {
            if (e.classList.contains('share-active')){
                if (el.target.classList.contains('share-active') || el.target.parentElement.hasAttribute('data-share-btn') || el.target.parentElement.tagName === 'svg') {
                    return false;
                } else {
                    e.classList.toggle('share-active');
                    shareBtn.forEach(z => {
                        if (z.classList.contains('project-active')){
                            z.classList.toggle('project-active')
                        }

                    })

                }
            }
        })

        //Адаптивное меню
        if (window.screen.width <= 768) {
            if (document.querySelector('body').classList.contains('phone-menu-active')) {
                if (!el.target.classList.contains('phone-header-active')) {
                    return false;
                } else { dropMenu(); }


            } else {
                if (phoneNumber.classList.contains('dropPhone2-active')) {
                    if (el.target.classList.contains('dropPhone2-active') || el.target.parentElement.classList.contains('phone2') || el.target.parentElement.hasAttribute('data-name') || el.target.classList.contains('phone2-svg')) {
                        return false;
                    } else {
                        numberDrop();
                    }
                }
            }
        }
    })


    document.addEventListener('keydown', (e) =>{
        if(e.key === "Escape"){
            if (searchInp.classList.contains('searchInpWidth')) {
                btnSearch()
            }
        }

    })

    navbarLinkPhone.forEach(el => {
        el.addEventListener('click', e => {
            let phoneMenu = e.target.dataset.phoneMenu;
            let navbarPhone = document.querySelector(`.navbar-phone[data-phone-menu="${phoneMenu}"]`)
            e.preventDefault();
            let svgNavbarPhone = document.querySelector(`svg[data-phone-menu="${phoneMenu}"]`)
            svgNavbarPhone.classList.toggle('svg-active')
            navbarPhone.classList.toggle('phone-ul');
        })
    })

//Выпадающее меню
    const dropMenu = () => {
        if (window.screen.width <= 768) {
            phoneMenu.classList.toggle('phone-header-active');
            document.querySelector("body").classList.toggle('phone-menu-active');
            backgroundMenu.classList.toggle('phone-backgroundActive')
        } else { DropDown.classList.toggle('DropDown'); }

        Burger1.classList.toggle('open1');
        Burger2.classList.toggle('open2');
        Burger3.classList.toggle('open3');
    }


    DropButton.addEventListener('click', dropMenu)

//CardPrice

    let priceCardBtn = document.querySelectorAll('.price-block-left-button');

    priceCardBtn.forEach(el => {
        el.addEventListener('click', e => {
            let dataAttr = el.dataset.cardBlockBtn
            document.querySelector(`.price-block-left-list[data-card-block-btn="${dataAttr}"]`).classList.toggle('price-block-left-list-active')
        })
    })

//Номера
    const numberDrop = () => {
        if (window.screen.width <= 768) {
            phoneNumber.classList.toggle('dropPhone2-active')
            if (document.querySelector('body').classList.contains('phone-menu-active')) {
                dropMenu();
                phoneNumber.classList.add('dropPhone2-active')
            }
        } else {
            dropPhone.classList.toggle('dropDownPhone');
            open.forEach(el => {
                el.classList.toggle('open-cursor');
            })
        }
    }

    open.forEach(el => {
        el.addEventListener("click", numberDrop)
    })

//Поиск
    function btnSearch() {
        for (let srcbtn of searchBtn) {
            srcbtn.classList.toggle('searchHidden');
        }
        searchInp.classList.toggle('searchInpWidth');
    }

    searchBtn.forEach(el => {
        el.addEventListener('click', btnSearch)
    })


//Скролл
    scrollUp.onclick = function () {
        window.scrollTo({
            top: 1,
            left: 0,
            behavior: "smooth",
        });
    }


//Swiper
    const swiper = new Swiper('.swiper', {
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiperTop-button-next',
            prevEl: '.swiperTop-button-prev',
        },
    });

//Swiper
    const swiper2 = new Swiper('.swiper2', {
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper2-button-next',
            prevEl: '.swiper2-button-prev',
        },
    });

//Swiper
    const swiper3 = new Swiper('.swiper3', {
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper3-button-next',
            prevEl: '.swiper3-button-prev',
        },
    });

    const swiper4 = new Swiper('.swiper4', {
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper4-button-next',
            prevEl: '.swiper4-button-prev',
        },
    });

    let filterActive = document.querySelectorAll('.filterForm');

//Filter
    function filterWatch() {
        for (let filtred of filterActive) {
            filtred.classList.toggle('filterActive');
        }
    }

//Поделиться
    shareBtn.forEach(el => {
        el.addEventListener('click', function (event){
            event.preventDefault()
            el.classList.toggle('project-active')
            let dataAttr = el.dataset.shareBtn;
            let shareList = document.querySelector(`.project-link[data-share-nav="${dataAttr}"]`);
            shareList.classList.toggle('share-active')
        })
    })
}

document.addEventListener("DOMContentLoaded", ready);