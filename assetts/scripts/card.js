const swiper5 = new Swiper('.swiper5', {
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper5-button-next',
        prevEl: '.swiper5-button-prev',
    },
});

const numberWithSpaces = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

let spanPrice = document.getElementById('price');
spanPrice.textContent = numberWithSpaces(4300000) + ' ₽';


let priceBtn = document.querySelectorAll('.price-btn');
priceBtn.forEach(el => {
    el.addEventListener('click', () => {
        document.querySelector('.price-btn-active').classList.remove('price-btn-active')
        el.classList.add('price-btn-active');
        let dataBlockId = el.dataset.priceBtn;
        document.querySelector('.active-price-block').classList.remove('active-price-block')
        document.querySelector(`.price-block[data-price-btn="${dataBlockId}"]`).classList.add('active-price-block')
    })
})
//Характеристики и дополнительно

const anotherBtn = () => {
    document.getElementById('card-ul-1').style.display = 'none';
    document.getElementById('card-ul-2').style.display = 'block'
    document.getElementById('anotherBtn').classList.add('card-btn-active');
    document.getElementById('btnFeature').classList.remove('card-btn-active');
}

const featureBtn = () => {
    document.getElementById('card-ul-1').style.display = 'block';
    document.getElementById('card-ul-2').style.display = 'none'
    document.getElementById('anotherBtn').classList.remove('card-btn-active');
    document.getElementById('btnFeature').classList.add('card-btn-active');
}

document.getElementById('btnFeature').onclick = featureBtn;
document.getElementById('anotherBtn').onclick = anotherBtn;


//LayoutForm
const layoutImg = () => {
    document.querySelector('.layout-img').style.display = 'grid'
    document.querySelector('.layout-form').style.display = 'none'
    document.getElementById('layoutbtn1').classList.add('layout-btn-active')
    document.getElementById('layoutbtn2').classList.remove('layout-btn-active')
}

const layoutForm = () => {
    document.querySelector('.layout-img').style.display = 'none'
    document.querySelector('.layout-form').style.display = 'flex'
    document.getElementById('layoutbtn1').classList.remove('layout-btn-active')
    document.getElementById('layoutbtn2').classList.add('layout-btn-active')
}

document.getElementById('layoutbtn1').onclick = layoutImg;
document.getElementById('layoutbtn2').onclick = layoutForm;

//загрузка файлов
let spanFile = document.getElementById('upload');
let fileInp = document.getElementById('file');
let a = 0;
spanFile.textContent = a + '';
fileInp.addEventListener('change', el => {
    a = fileInp.files.length;
    spanFile.textContent = a + '';
})

if (window.screen.width <= 768) {
    document.querySelector('.card-price').remove();
} else {
    document.querySelector('.card-price-mobile').remove()
}

const swiper = new Swiper('.card-swiper', {
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '',
        prevEl: '',
    },
});

