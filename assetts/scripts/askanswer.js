let askBtn = document.querySelectorAll('.top5-block-list-button');

askBtn.forEach(el => {
    el.addEventListener('click', () => {
        let dataAsk = el.dataset.askButton;
        document.querySelector(`.top5-block-list[data-ask-button='${dataAsk}']`).classList.toggle('top5-block-list-active')
    })
})