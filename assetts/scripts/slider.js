let slider = document.getElementById('slider');
let slider2 = document.getElementById('slider2');

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

//range-slider 1
noUiSlider.create(slider, {
    start: [1738235, 2738235],
    connect: true,
    step: 1000,
    range: {
        'min': 1500000,
        'max': 3000000,
    },

});

//range-slider2 2

noUiSlider.create(slider2, {
    start: [50, 120],
    connect: true,
    step: 1,
    range: {
        'min': 30,
        'max': 150,
    }
});

//range-slider txt 1
let input0 = document.getElementById('input0');
let input1 = document.getElementById('input1');
let inputs1 = [input0, input1];

slider.noUiSlider.on('update', function(values, handle){
    inputs1[handle].value = numberWithSpaces(Math.round(values[handle]));

})




const setRangeSlider = (i, value) => {
    let arr = [null, null];
    arr[i] = value;

    slider.noUiSlider.set(arr);
};

inputs1.forEach((el, index) => {
    el.addEventListener('change', (e) => {
        setRangeSlider(index, e.currentTarget.value);
    });
})

//range-slider txt 2
let input2 = document.getElementById('input2');
let input3 = document.getElementById('input3');
let inputs2 = [input2, input3];
slider2.noUiSlider.on('update', function(values, handle){
    inputs2[handle].value = Math.round(values[handle]);
})

const setRangeSlider2 = (i, value) => {
    let arr = [null, null];
    arr[i] = value;

    slider2.noUiSlider.set(arr);
};

inputs2.forEach((el, index) => {
    el.addEventListener('change', (e) => {
        setRangeSlider2(index, e.currentTarget.value);
    });
})
