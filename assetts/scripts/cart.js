const projectBtn = document.querySelectorAll('.project-like');
let heartCounter = document.querySelector('.heart');
const heart = document.querySelector('.heart-active');
let favCounter = heartCounter.dataset.likeValue;
heartCounter.textContent = favCounter;


//Отправка если none active
async function favAdd(id){
    let likeId = new FormData();
    likeId.append('id', id);
    fetch('',{
        method: "POST",
        body: likeId,
    }).then(res => {
        if (res.ok) {
            ++favCounter;
            heartCounter.textContent = favCounter;
            counterCheck();
        } else {
            alert('Произошла непредвиденная ошибка ')
            console.log(res.code);
        }
    }).catch(err => {
        alert('Произошла непредвиденная ошибка')
        console.log(err);
    });
}

//отправка если active
async function favRem(id){
    let likeId = new FormData();
    likeId.append('id', id);
    fetch('',{
        method: "POST",
        body: likeId,
    }).then(res => {
        if (res.ok) {
            --favCounter;
            heartCounter.textContent = favCounter;
            counterCheck();
        } else {
            alert('Произошла непредвиденная ошибка ')
            console.log(res.code);
        }
    }).catch(err => {
        alert('Произошла непредвиденная ошибка')
        console.log(err);
    });
}

//проверка counter
function counterCheck() {
    if (favCounter > 0) {
        heartCounter.classList.add('added');
        heart.classList.add('heart-activeAdd');
    } else {
        heartCounter.classList.remove('added');
        heart.classList.remove('heart-activeAdd');
    }
}

projectBtn.forEach(el => {
    el.addEventListener('click', (e) => {
        e.preventDefault();
        let id = el.dataset.likeId;
        el.classList.toggle('project-active');
        if (el.classList.contains('project-active')) {
            favAdd(id);
        } else {
            favRem(id);
        }
    })
})


