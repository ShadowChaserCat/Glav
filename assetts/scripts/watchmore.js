let watchMore = document.querySelector('.swiper2WatchMore');
let page = 2;
let btnWork = true;

function lookMore() {
    let pageNumber = new FormData();
    pageNumber.append('page', page);

    if (btnWork === true) {
        btnWork = false;
        watchMore.disabled = true;
    } else {
        watchMore.disabled = false;
    }

    fetch('',{
        method: "POST",
        body: pageNumber,
    }).then(async res => {
        if (res.ok) {
            let result = await res.json();
            result.items.forEach(item => {
                //Проверка
                let itemHTML = firstProject(item.dataShare, item.title, item.txt, item.link, item.img, item.name);
                document.querySelector('.project-block').insertAdjacentHTML('beforeend', itemHTML)
                if (result.isEnd) {
                    watchMore.remove();
                } else {
                    page++;
                    setTimeout(function () {
                        btnWork = true;
                    }, 500)
                }
                }
            )
            firstProject();
        } else {
            alert('Что-то пошло не так!');
            console.error(res.code);
        }
    }).catch(err => {
        alert('Произошла непредвиденная ошибка')
        console.log(err);
    });
}


const firstProject = (dataShare, title, txt, link, img, name) => {
    return `
    <div  class="swiper2Title">
            <div class="swiper2Left">
                <div class="swiper2LeftBlock">
                    <div class="swiper2LeftTop">
                        <h4>${title}</h4>
                        <p>${txt}</p>
                    </div>
                    <a href="#"> Все проекты</a>
                </div>
            </div>
            <div class="swiper2Right">
                <div class="swiper2">
                    <div class="swiper-wrapper">
                        <article class="swiper-slide">
                            <a class="swiper-link" href="${link}">
                                <img class="swiper2IMG" src="${img}" alt="#">
                                <div class="swiper2-img-name">${name}</div>
                            </a>
                        </article>
                    </div>
                </div>
                <div class="swiper2RightBottom">
                    <div class="swiper2Icon">
                        <button class="project-share" data-share-btn="${dataShare}">
                            <svg width="41" height="40" viewBox="0 0 41 40" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M21.3793 4.83982L26.7895 10.6173C27.1475 10.9981 27.0967 11.5711 26.6719 11.8951C26.4849 12.0402 26.2551 12.1103 26.0254 12.1103C25.7408 12.1103 25.459 12.0015 25.2613 11.79L21.3499 7.6119V23.9313C21.3499 24.433 20.9011 24.838 20.348 24.838C19.795 24.838 19.3461 24.433 19.3461 23.9313V7.62278L15.4348 11.7996C15.2371 12.0112 14.9539 12.1188 14.6707 12.1188C14.4409 12.1188 14.2111 12.0487 14.0228 11.9036C13.6006 11.582 13.5485 11.009 13.9065 10.627L19.3181 4.84829L19.3394 4.82411C19.4423 4.70563 19.807 4.28613 20.348 4.28613C20.8997 4.28613 21.2537 4.69354 21.3579 4.81564L21.3793 4.83982ZM8.99327 32.3902C8.99327 34.2241 10.6404 35.7147 12.6669 35.7147H28.0291C30.0556 35.7147 31.7027 34.2241 31.7027 32.3902V18.4875C31.7027 16.6535 30.0556 15.1629 28.0291 15.1629H25.3347V16.9763H28.0291C28.9495 16.9763 29.6989 17.6545 29.6989 18.4875V32.3902C29.6989 33.2231 28.9495 33.9013 28.0291 33.9013H12.6669C11.7465 33.9013 10.997 33.2231 10.997 32.3902V18.4875C10.997 17.6545 11.7465 16.9763 12.6669 16.9763H15.4174V15.1629H12.6669C10.6404 15.1629 8.99327 16.6535 8.99327 18.4875V32.3902Z"
                                      fill="#D9DBE1"/>
                            </svg>
                        </button>
                        <nav data-share-nav="${dataShare}" class="project-link">
                            <button
                                    class="btn-share vkontakte"
                                    onClick="window.open('https://vkontakte.ru/share.php?url=https://only-to-top.ru/blog/coding/2019-11-01-knopki-podelitsya-dlya-sajta.html','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Поделиться Вконтакте">
                                ВКонтакте
                            </button>
                            <button
                                    class="btn-share facebook"
                                    onClick="window.open('https://www.facebook.com/sharer.php?u=АДРЕС_СТРАНИЦЫ','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Поделиться в Facebook">
                                Facebook
                            </button>
                            <button
                                    class="btn-share telegram"
                                    onClick="window.open('https://telegram.me/share/url?url=АДРЕС_СТРАНИЦЫ','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Поделиться в Телеграм">
                                Telegram
                            </button>
                            <button
                                    class="btn-share twitter"
                                    onClick="window.open('https://twitter.com/intent/tweet?text=ТАЙТЛ_СТРАНИЦЫ. ОПИСАНИЕ. СССЫЛКА','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Твитнуть">
                                Twitter
                            </button>
                            <button
                                    class="btn-share whatsapp"
                                    onClick="window.open('whatsapp://send?text=ТАЙТЛ_СТРАНИЦЫ. ОПИСАНИЕ. СССЫЛКА','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Поделиться в WhatsApp">
                                WhatsApp
                            </button>
                            <button
                                    class="btn-share viber"
                                    onClick="window.open('viber://forward?text=GlavStroy365   ОПИСАНИЕ. СССЫЛКА','sharer','status=0,toolbar=0,width=650,height=500');return false;"
                                    title="Поделиться в Viber">
                                Viber
                            </button>
                        </nav>
                    </div>
                    <div class="swiper2Icon">
                        <svg class="swiper2-button-prev" width="40" height="40" viewBox="0 0 40 40" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.4572 32L26.1038 29.3167L17.4079 20.5L26.1038 11.6833L23.4572 9L12.1147 20.5L23.4572 32Z"
                                  fill="#D9DBE1"/>
                        </svg>
                        <svg class="swiper2-button-next" width="40" height="40" viewBox="0 0 40 40" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.6988 32L12.0522 29.3167L20.7482 20.5L12.0522 11.6833L14.6988 9L26.0414 20.5L14.6988 32Z"
                                  fill="#D9DBE1"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    `;
}

watchMore.onclick = lookMore;